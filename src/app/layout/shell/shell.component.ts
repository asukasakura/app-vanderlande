import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.pug',
  styleUrls: ['./shell.component.styl']
})
export class ShellComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
