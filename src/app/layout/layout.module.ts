import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './shell/header/header.component';
import { NavComponent } from './shell/nav/nav.component';
import { AsideComponent } from './shell/aside/aside.component';
import { FooterComponent } from './shell/footer/footer.component';
import { MainComponent } from './shell/main/main.component';
import { ShellComponent } from './shell/shell.component';



@NgModule({
  declarations: [HeaderComponent, NavComponent, AsideComponent, FooterComponent, MainComponent, ShellComponent],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent, NavComponent, AsideComponent, FooterComponent, MainComponent, ShellComponent]
})
export class LayoutModule { }
